#include "transfers.h"

#include "diag/Trace.h"
#include <string.h>
#include <assert.h>
// iota-related stuff
#include "conversion.h"
#include "addresses.h"
#include "bundle.h"
#include "signing.h"
#include "../aux.h"

extern uint32_t systick;
extern Keccak384* pKeccak384;
extern Conversion* pConversion;

static void get_address(const unsigned char *seed_bytes, uint32_t idx, unsigned int security, char *address) {
	unsigned char bytes[48];
	get_public_addr(seed_bytes, idx, security, bytes);
	pConversion->bytes_to_chars(bytes, address, 48);
}

Transaction* prepare_transfers(BUNDLE_CTX* bundle_ctx, char *seed, uint8_t security, Transaction *txs, int txs_len, int max_txs) {
	// TODO use a proper timestamp
	const uint32_t timestamp = 0;//systick;

	int num_txs = 0;
	for (int i = 0; i < txs_len; i++) {
		switch (txs[i].getType()) {
		case Transaction::Input:
			num_txs += security;
			break;
		case Transaction::Output:
			num_txs++;
			break;
		default: // empty
			break;
		}
	}

	const unsigned int last_tx_index = num_txs - 1;

	// append signatures after input and output TXs
	int empty = last_tx_index + 1;

	if (num_txs > max_txs)
		return 0;

	unsigned char seed_bytes[48];
	if (seed)
		pConversion->chars_to_bytes(seed, seed_bytes, 81);

	Transaction* prev = 0;
	Transaction* first = 0;
	// first create the transaction objects
	for (int i = 0; i < txs_len; i++) {
		if (txs[i].getType() != Transaction::Output)
			continue;

		if (!first) {
			first = &txs[i];
		}

		// important for bundle finalization
		txs[i].setObsoleteTag(txs[i].getTag());

		txs[i].setTimestamp(timestamp);
		txs[i].setLastIndex(last_tx_index);
		txs[i].setPrev(prev);
		prev = &txs[i];
	}

	for (int i = 0; i < txs_len; i++) {
		if (txs[i].getType() != Transaction::Input)
			continue;
		assert(txs[i].getAddressIndex() != 0xffffffff);

		get_address(seed_bytes, txs[i].getAddressIndex(), security, txs[i].getAddress());

		txs[i].setTimestamp(timestamp);
		txs[i].setLastIndex(last_tx_index);
		txs[i].setPrev(prev);
		prev = &txs[i];
		for (uint32_t j = 1; j < security; j++) {
			txs[empty].setAddressIndex(txs[i].getAddressIndex());
			txs[empty].setAddress(txs[i].getAddress());
			txs[empty].setType(Transaction::Signature);
			txs[empty].setValue(0);
			txs[empty].setTimestamp(timestamp);
			txs[empty].setLastIndex(last_tx_index);
			txs[empty].setPrev(prev);
			prev = &txs[empty];
			empty++;
		}
	}

	bundle_initialize(bundle_ctx, last_tx_index);

	Transaction* tx = first;
	int idx = 0;
	do {
		tx->setCurrentIndex(idx);
		bundle_set_external_address(bundle_ctx, tx->getAddress());
		bundle_add_tx(bundle_ctx, tx->getValue(), tx->getObsoleteTag(), tx->getTimestamp());
		tx = tx->getNext();
		idx++;
	} while (tx);

	// create a secure bundle


	uint32_t tag_increment = bundle_finalize(bundle_ctx);

	// increment the tag in the first transaction object
	first->increment_obsolete_tag(tag_increment);

	// set the bundle hash in all transaction objects
	char bundle[81];
	pConversion->bytes_to_chars(bundle_get_hash(bundle_ctx), bundle, 48);

	tx = first;
	do {
		tx->setBundle(bundle);
		tx->convertIntValues();
		tx = tx->getNext();
	} while (tx);
	//set_bundle_hash(bundle_ctx, txObjects, num_txs);

	// sign the inputs
	tryte_t normalized_bundle_hash[81];
	bundle_get_normalized_hash(bundle_ctx, normalized_bundle_hash);

	if (seed) {
		Signing signing(pKeccak384, seed_bytes, tx->getAddressIndex());
		tx = first;
		do {
			if (tx->getType() == Transaction::Input) {
				for (uint32_t j = 0; j < security; j++) {
					signing.sign(tx, &normalized_bundle_hash[j*27]);
					tx = tx->getNext();
				}
			} else {
				tx = tx->getNext();
			}
		} while (tx);
	}

	tx = first;
	do {
		tx->convertIntValues();

		// reverse linked list
		tx->reverseLink();
		first = tx;

		// use here prev instead of next
		tx = tx->getPrev();

//		tx = tx->getNext();
	} while (tx);

	return first;
}
