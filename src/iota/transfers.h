#ifndef TRANSFERS_H
#define TRANSFERS_H

#include <stdint.h>

#include "bundle.h"
#include "transaction.h"



#ifdef __cplusplus
extern "C" {
#endif



Transaction* prepare_transfers(BUNDLE_CTX* bundle_ctx, char *seed, uint8_t security, Transaction* txs, int txs_len, int max_txs);

#ifdef __cplusplus
}
#endif

#endif //TRANSFERS_H
