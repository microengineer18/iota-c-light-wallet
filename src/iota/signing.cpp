#include "signing.h"
#include "common.h"
#include "conversion.h"
#include "kerl.h"

extern Conversion* pConversion;


Signing::Signing(Keccak384* keccak, uint8_t* seedBytes, uint32_t addressIndex) {
	m_keccak = keccak;
	m_addressIndex = addressIndex;

	memcpy(m_state, seedBytes, 48);

	pConversion->bytes_add_u32_mem(m_state, m_addressIndex);

    kerl_initialize(m_keccak);
    kerl_absorb_chunk(m_keccak, m_state);
    kerl_squeeze_final_chunk(m_keccak, m_state);
}

bool Signing::sign(Transaction* tx, tryte_t* bundleHashFragment) {
    kerl_reinitialize(m_keccak, m_state);

    char* pSignatureMessageFragment = tx->getsignatureMessageFragment();
    for (unsigned int j = 0; j < 27; j++) {
        uint8_t signatureBytes[48]={0};

        kerl_reinitialize(m_keccak, m_state);
        // the output of the squeeze is exactly the private key
        kerl_state_squeeze_chunk(m_keccak, m_state, signatureBytes);

        for (unsigned int k = MAX_TRYTE_VALUE - bundleHashFragment[j]; k-- > 0;) {
            kerl_initialize(m_keccak);
            kerl_absorb_chunk(m_keccak, signatureBytes);
            kerl_squeeze_final_chunk(m_keccak, signatureBytes);
        }
    	pConversion->bytes_to_chars(signatureBytes, pSignatureMessageFragment, 48);
    	pSignatureMessageFragment += 81;
    }
    return true;
}

